# SPDX-FileCopyrightText: 2023-2024 Simon Repp
# SPDX-License-Identifier: GPL-3.0-or-later

import argparse
import math
import os
import pathlib
import re
import shutil
import subprocess
import sys

def exfatize_path(path):
    parts = []
    for part in pathlib.Path(path).parts:
        parts.append(exfatize_name(part))

    return os.path.join(*parts)

def exfatize_name(name):
    if name.endswith("."):
        return f"{name}_exfat"
    else:
        return name

parser = argparse.ArgumentParser(add_help=False)

parser.add_argument('source_dir')

parser.add_argument('target_dir')

parser.add_argument('--exfat',
                    action='store_true',
                    help='Enable exFAT compatibility mode (fix glitches with restricted filenames)')

parser.add_argument('--retranscode',
                    action='store_true',
                    help = 'Force a retranscoding of transcoded files that already exist at the target directory')

parser.add_argument('--simulate',
                    action='store_true',
                    help = 'Perform a simulation, printing what file changes would be performed without performing them')

parser.add_argument('--transcode',
                    help = 'Comma-separated list of formats that should be transcoded on-the-fly',
                    type = str)

parser.add_argument('--transcode-to',
                    help = 'Target format for all files that should be transcoded.',
                    type = str)

parser.add_argument('--verbose',
                    action='store_true',
                    help = 'Display more granular messages about the syncing progress (= additions of sub(-sub(-etc.))-entries)')

args = parser.parse_args()

source_dir = os.path.abspath(args.source_dir)
target_dir = os.path.abspath(args.target_dir)

format_mapping = {}

if args.transcode and args.transcode_to:
    for in_extension in args.transcode.split(","):
        format_mapping[in_extension] = args.transcode_to

source_root_entries = {}
target_root_entries = {}

# Recursively scans directory starting at dir.
# The dict entries is filled with the results.
# If recode_dict is passed, all files whose extension
# shows up in the recode_dict get remapped, such that
# e.g. the key for the file "foo.opus" will be "opus.mp3"
# when remapping opus to mp3.
def recursive_scan(dir, entries, recode_dict, root_dir, exfat=False):
    for dir_entry in os.scandir(dir):
        # Our source and target directory can be on different file systems.
        # Mtime precision varies between different file systems, hence we
        # normalize the mtime to seconds granularity, so we can safely compare.
        mtime = math.floor(dir_entry.stat().st_mtime)

        entry = {
            "abspath": dir_entry.path,
            "mtime": mtime,
            "relpath": os.path.relpath(dir_entry.path, start=root_dir)
        }

        if dir_entry.is_dir():
            entry["subentries"] = {}
            entry["type"] = "dir"

            if exfat:
                entry["virtual_relpath"] = exfatize_path(entry["relpath"])
                entries[exfatize_name(dir_entry.name)] = entry
            else:
                entry["virtual_relpath"] = entry["relpath"]
                entries[dir_entry.name] = entry

            recursive_scan(
                entry["abspath"],
                entry["subentries"],
                recode_dict,
                root_dir,
                exfat
            )
        elif dir_entry.is_file():
            entry["type"] = "file"

            extension = os.path.splitext(entry["abspath"])[1][1:]
            if recode_dict and extension in recode_dict:
                entry["transcode"] = True
                name_recoded = re.sub(f"{extension}$", recode_dict[extension], dir_entry.name)
                if exfat:
                    entry["virtual_relpath"] = exfatize_path(re.sub(f"{extension}$", recode_dict[extension], entry["relpath"]))
                else:
                    entry["virtual_relpath"] = re.sub(f"{extension}$", recode_dict[extension], entry["relpath"])
                entries[name_recoded] = entry
            else:
                if exfat:
                    entry["virtual_relpath"] = exfatize_path(entry["relpath"])
                    entries[exfatize_name(dir_entry.name)] = entry
                else:
                    entry["virtual_relpath"] = entry["relpath"]
                    entries[dir_entry.name] = entry

# Pass a dir or file and it will be copied or transcoded (as applies)
# from source to target directory. With directories this performs
# recursive calls of itself.
def copy_transcode(source_entry):
    if source_entry["type"] == "file":
        verbose_progress_message(f"Adding {source_entry['virtual_relpath']}")

        if args.simulate:
            return

        if "transcode" in source_entry:
            target_abspath = os.path.join(target_dir, source_entry["virtual_relpath"])

            subprocess.run([
                "ffmpeg",
                "-i", source_entry["abspath"],
                # Copy metadata from first audio stream to output.
                # Necessary because some conversion do not automatically carry
                # over the metadata (e.g. was observed for opus to mp3).
                "-map_metadata", "0:s:a:0",
                target_abspath
            ], capture_output=True)

            # Copy filesystem metadata from source file to recoded file (so they have the same mtime)
            subprocess.run(["touch", "-r", source_entry["abspath"], target_abspath])
        else:
            target_abspath = os.path.join(target_dir, source_entry["virtual_relpath"])

            # copy2 copies filesystem metadata alongside file content (so they have the same mtime)
            shutil.copy2(source_entry["abspath"], target_abspath)
    else:
        target_abspath = os.path.join(target_dir, source_entry["virtual_relpath"])

        if not args.simulate:
            os.mkdir(target_abspath)

        for subentry in source_entry["subentries"].values():
            verbose_progress_message(f"Adding {subentry['virtual_relpath']}")
            copy_transcode(subentry)

def progress_message(text):
    if args.simulate:
        print(f"[SIMULATED] {text}")
    else:
        print(text)

def recursive_sync(source_entries, target_entries):
    for source_name, source_entry in source_entries.items():
        if source_name in target_entries:
            target_entry = target_entries[source_name]

            target_entry["used"] = True

            if source_entry["type"] != target_entry["type"]:
                progress_message(f"Replacing {target_entry['relpath']}")

                if not args.simulate:
                    os.remove(target_entry["abspath"])

                copy_transcode(source_entry)
            elif source_entry["type"] == "dir":
                recursive_sync(source_entry["subentries"], target_entry["subentries"])
            elif source_entry["type"] == "file":
                if source_entry["mtime"] != target_entry["mtime"] or (args.retranscode and "transcode" in source_entry):
                    progress_message(f"Replacing {target_entry['relpath']}")

                    if not args.simulate:
                        os.remove(target_entry["abspath"])

                    copy_transcode(source_entry)
        else:
            progress_message(f"Adding {source_entry['virtual_relpath']}")
            copy_transcode(source_entry)

# Never ever pass source_entries into this (!)
def recursive_clean(target_entries):
    for target_entry in target_entries.values():
        if not "used" in target_entry:
            progress_message(f"Deleting {target_entry['relpath']}")

            if not args.simulate:
                if target_entry["type"] == "dir":
                    shutil.rmtree(target_entry["abspath"])
                else:
                    os.remove(target_entry["abspath"])
        elif target_entry["type"] == "dir":
            recursive_clean(target_entry["subentries"])

def verbose_progress_message(text):
    if args.verbose:
        progress_message(text)

recursive_scan(source_dir, source_root_entries, format_mapping, source_dir, exfat=args.exfat)
recursive_scan(target_dir, target_root_entries, None, target_dir)

recursive_sync(source_root_entries, target_root_entries)
recursive_clean(target_root_entries)
