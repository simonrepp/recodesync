<!--
    SPDX-FileCopyrightText: 2023-2024 Simon Repp
    SPDX-License-Identifier: CC0-1.0
-->

# recodesync.py

Classic audio formats such as MP3 offer the advantage of being playable on
the widest range of devices. Newer audio formats such as OPUS on the other
hand offer significantly better compression. What if I want to store my
audio library using state-of-the-art formats, but still want to be able to
play my library from an old device that does not support state-of-the-art
formats? recodesync.py is an rsync-like CLI tool to synchronize your
library from your storage device (where you keep state-of-the-art encoded
audio files) to your playback device (which only supports older formats),
while automatically transcoding files for playback compatibility as part
of the syncing process.

For instance, imagine a tiny music library in your home folder:

```
$ ls ~/Music
foo.opus bar.flac baz.mp3
```

Opus and Flac are great formats, but imagine your mp3 player only plays mp3, so we use recodesync:

```
$ python recodesync.py ~/Music /run/media/sdcard --transcode flac,opus --transcode-to mp3
Adding foo.mp3
Adding bar.mp3
Adding baz.mp3
```

And this is what you now find on your MP3 player:

```
$ ls /run/media/sdcard
foo.mp3 bar.mp3 baz.mp3
```

## exFAT Compatibility mode

If you're syncing e.g. from ext4 to a storage medium that uses exFAT, you
might run into issues around allowed dir/file names. By passing the optional
`--exfat` flag, recodesync will rename restricted filenames
like "Endswithadot." to "Endswithadot.\_exfat" on the target storage medium
on the fly during syncing.

## Simulated runs

By passing `--simulate` you can run a simulation of the sync that prints all actions that would be
performed, but does not actually perform any of them. You are encouraged to use this to avoid accidents
with wrong paths etc. :)

## WARNING

As a sync tool, this tool can delete a lot of your data, in an instant. Pay attention what arguments
you pass. Make backups of your data beforehand. If bugs occur, please let me know so we can fix them.

## Quick usage reference 

```
python recodesync.py [source_dir] [destination_dir] --transcode foo,bar --transcode-to baz

# e.g.
python recodesync.py /home/alice/Music /run/media/sdcard --transcode aac,opus --transcode-to mp3

```

## Usage

Assume Alice keeps her music in `/home/alice/Music`, stored as `.opus` and `.aac` files.
Assume the SD card of her old MP3 player is mounted at `/run/media/sdcard`. 
Alice would run this command:

```
python recodesync.py /home/alice/Music /run/media/sdcard --transcode aac,opus --transcode-to mp3
```

After completion, `/run/media/sdcard` would contain her music, but encoded
exclusively as `.mp3` files. On future runs, only those files that have
been added/updated/moved/removed, will be re-synced. (Note that on a technical level
the `mtime` is used to mark and determine whether files need updates)

## Verbose progress information

By default when syncing to a target directory, a line like "Adding example.mp3" is printed
when something does not exist in the target directory and is copied there. In the case of
directories, this only happens for the top-most directory that is missing, but not for any
subentries. By passing `--verbose` this information will be printed for each and every
directory and files that is newly added.

## Forcing retranscode

By passing `--retranscode` you can force a retranscode of all transcoded files that already
exist in the target directory. This can be useful e.g. after updates to recodesync that
update the transcoding behavior.

# License

recodesync.py is licensed under the [GPL-3.0-or-later](https://spdx.org/licenses/GPL-3.0-or-later.html).
